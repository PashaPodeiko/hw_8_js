const input = document.querySelector(".text_input_field"),
	clearBtn = document.querySelector(".clear_btn"),
	curentPriceSpan = document.querySelector(".curent_price"),
	errorPrice = document.querySelector(".error_price"),
	newSpan = document.createElement("span");


input.addEventListener("focus", () => { //появиться рамка зеленого цвета
	input.classList.add("input_focus");
});

clearBtn.addEventListener("mousedown", () => {
	input.value = "";
	clearBtn.style.display = "none";
	newSpan.remove();
	errorPrice.style.opacity = "0";
	input.classList.add("input_focus");
});

input.addEventListener("blur", () => {
	input.classList.remove("input_focus");
	if (input.value && input.value < 0) {
		errorPrice.style.opacity = "1";
		newSpan.remove();
		input.classList.add("error_text_input_field");

	} else if (input.value) {
		displayCurentPrice(input.value);
		errorPrice.style.opacity = "0";
	}
});

input.addEventListener("input", () => { // при вводе в input появл. Х	
	clearBtn.style.display = "block";
});

function displayCurentPrice(value) {
	if (curentPriceSpan == null) {
		newSpan.classList.add("curent_price");
		newSpan.innerHTML = `Текущая цена: ${value}`;
		document.body.prepend(newSpan);
	} else {
		curentPriceSpan.innerHTML = `Текущая цена: ${value}`;
	}
}

